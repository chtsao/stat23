---
title: "About"
date: 2023-02-08T12:41:00+08:00
draft: false
categories: [adm]
---
<img src="https://images.fineartamerica.com/images-medium-large/parisian-salon-granger.jpg" alt="Dice" style="zoom:50%;" />

* Curator: Kno Tsao.  Office: SE A411.  Tel: 3520
* Lectures: Tue. 1300*-1500, Thr. 1605-1705* @ AE A210
* [Google Classroom](https://classroom.google.com/c/NTk1MjQ1ODE4Njk5?cjc=b3fc677); [Google Meet](https://meet.google.com/smh-ggea-jqt)(just for "rainy" days).
* Office Hours: Thr 13:10-15:00  @ SE A411
* TA Office Hours: 
  * 呂一昕：Thursdays 1700-1900 ＠A412
  * 施承翰：Tuesdays 1700-1900 @A408
* Prerequisites: Calculus, Intro to Probability
* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legally downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)
##### Ref's and Links
  - [Paccioli's puzzle (Luca Pacioli)](https://en.wikipedia.org/wiki/Luca_Pacioli) NBA final, Accounting and DaVinci?
  - [How the Chevalier de Méré met Blaise Pascal](https://stevehely.com/2021/02/21/how-the-chevalier-de-mere-met-blaise-pascal/)... Says who? Charlie Munger?
