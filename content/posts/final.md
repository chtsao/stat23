---
title: "Finallly.. Final"
date: 2023-06-02T08:51:38+08:00
draft: false
---

 <img src="https://f4.bcbits.com/img/a0288819336_10.jpg" alt="Running" style="zoom:50%;" />

### Final Exam
* 日期: 2023 0606 (二)
* 時間: 1310-1440.   地點: A210
* 範圍：上課及習題內容。對照課本章節約為: Chapter 21; Chapter 23 (~~23.3~~); Chapter 24; Sec. 26.1, Chapter 27 (~~27.3~~). Sec. 28.1, conepts/example of two-sample and paired design when comparing two normal means. Paired-t test. (注意： Bernoulli population mean $p$ testing, two-sample t-test, p-value 不考）
* 考古題/範例(NEW): [2020](https://chtsao.gitlab.io/stat23/ksfin2020.pdf), 2021 ([html](https://chtsao.gitlab.io/stat23/fin2021.html), [pdf](https://chtsao.gitlab.io/stat23/fin2021.pdf)) 。請注意之前範圍與形式與本次考試不盡相同。請參考考試範圍。另外，請同學先就筆記/課本/習題內容複習，自己或與同學Quizz 後再試做。比起直接做考古題效果會好很多。
* 其他：No cheatsheet nor mobile phone. Prepare early and Good Luck!



提早準備，固實會的，加強生疏的，弄懂原來不會的！----考試不難，會就簡單！

### Week 18 Supplemental (NEW)

* 暫訂於 6/~~13~~14 (三) 10:00-1200 線上方式進行。有興趣的同學參加，如很想聽但時間衝突，請email 和我說。
* [Google Meet link](https://meet.google.com/rbf-crbb-yav)
* 內容：
	* p-value 
	* Testing for Bernoulli p 轉蛋法與丁特/紫布事件