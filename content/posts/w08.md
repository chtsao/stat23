---
title: "Week 8. Midterm Alert"
date: 2023-04-07T05:05:36+08:00
draft: false
---
![](https://th.bing.com/th/id/R.3e41b5217f4236961d5b2c97c2c87f45?rik=cXIm5ZBLeXkpSQ&riu=http%3a%2f%2f3.bp.blogspot.com%2f-12FJZL0YETY%2fVj7Nl3WCfGI%2fAAAAAAAAABo%2fZ8BvcyUzt0I%2fs1600%2fexams..jpg&ehk=%2fXHL7F3OsCuyfVOgVUAscPTlDgiDI%2bzq%2bihIPjJldR0%3d&risl=&pid=ImgRaw&r=0)

# Midterm

* 日期: 2021 4/26 （二）.
* 地點 (NEW)：A210 
* 時間: 1310-1440.
* 範圍：上課及習題內容。建議參考上課筆記與課網大綱。
* 其他：No cheatsheet nor mobiles allowed. Prepare early and Good Luck! 提早準備，固實會的，加強生疏的，弄懂原來不會的！—-考試不難，會就簡單！
* (**NEW**) Old exams for your practice: [Variation on 2020 exam](https://chtsao.gitlab.io/stat23/vmid20.pdf), [2022](https://chtsao.gitlab.io/stat23/mid22.pdf) (Skip Problem 2 ~~and Problem 6~~).

## Prepare and be ready