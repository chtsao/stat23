---
title: "Midterm Summary"
date: 2023-04-19T05:22:49+08:00
draft: false
tags: [midterm, remark]
---
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fquotefancy.com%2Fmedia%2Fwallpaper%2F3840x2160%2F1700804-Aristotle-Quote-We-are-what-we-repeatedly-do-Excellence-therefore.jpg&f=1&nofb=1" style="zoom:50%;" />

#### [How To Make Excellence A Habit](https://www.forbes.com/sites/forbescoachescouncil/2019/11/12/how-to-make-excellence-a-habit/?sh=e047b262aedf)  

### Midterm

Five-point summary:

```
##    Min.    Q1     Median   Mean   Q3      Max. 
##   12.00   22.00   39.00   40.29   57.00   93.00
```
$\bar{x}= 40.29$, $s=20.8$; $n^*=41$; 年級 2 = 30; 年級 3+ = 11. (缺考8人; 10.1分以下 15人)

Stem-and-leaf
```
 The decimal point is 1 digit(s) to the right of the |
## 
##    9 | 3
##    8 | 
##    7 | 32
##    6 | 8663311
##    5 | 765
##    4 | 7755310
##    3 | 986531
##    2 | 64322110
##    1 | 5555522
```
[進一步圖表分析](https://chtsao.gitlab.io/stat23/midrv.html), 鑑往*知*來([2016stat](http://faculty.ndhu.edu.tw/~chtsao/ftp/rg.w08.nb.html), [2019I2p](http://faculty.ndhu.edu.tw/~chtsao/ftp/i2p/i2p2019w.nb.html), [2020stat](http://faculty.ndhu.edu.tw/~chtsao/ftp/i2p/stat20w.nb.html),[2021stat](https://chtsao.gitlab.io/stat21/posts/w09/)), [2022stat](https://chtsao.gitlab.io/stat22/posts/midsum/) 

### Remark
1. 考得好的同學：保持這次的優勢，繼續努力，嘗試對這主題有更深的了解。讓這門課成為你**知道真正知道**的起點。
2. 考得不理想的同學：現在放棄，你等於提早出局。~~我教的課，不論期中多低分，都有過的機會~~ 不要低估自己的可能，但更不要低估自己的惰性。有時偷點懶是人的天性，但不能放縱慣養惰性，讓自己成為它的奴隸。這門課就是一個與它的對戰。[成功與失敗都不是一個結果，而是習慣](http://smilyanp.com/success/)。課程及格不及格當然有影響，但慢慢養成（促成）成功的習慣，遠離失敗的習慣，更為重要。
3. [關於棄選](http://faculty.ndhu.edu.tw/~chtsao/ftp/i2p/withdrawl.html) (注意！)

> 學習成效不好的人，很多不是沒有花時間讀書，而是不知道怎麼讀書----不知道怎麼讀得有效，獨得開心

###  Study less, study smart
* Study Less Study Smart”by Dr. Marty Lobdell: [summary by UAPB](https://www.uapb.edu/sites/www/Uploads/SSC/Study%20Smarter%20Not%20Harder.pdf), [video](https://www.youtube.com/watch?v=IlU-zDU6aQ0)
* [學得更好-更開心-更有效率](https://chtsao.gitlab.io/i2p2019/#%E4%BD%A0%E5%8F%AF%E4%BB%A5%E5%AD%B8%E5%BE%97%E6%9B%B4%E5%A5%BD-%E6%9B%B4%E9%96%8B%E5%BF%83-%E6%9B%B4%E6%9C%89%E6%95%88%E7%8E%87)
*  Mike and Matty: [The REAL Reason Why You Get Bad Grades](https://youtu.be/GJ_o-1bfz-M), [Evidence based learning strategies](https://youtu.be/UEJmgaFQUH8)
