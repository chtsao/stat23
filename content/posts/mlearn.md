---
title: "About Learning"
date: 2023-02-15T16:26:41+08:00
draft: false
tags: [notes, learn]
---

<img src="https://ddragon.leagueoflegends.com/cdn/img/champion/splash/MasterYi_42.jpg" style="zoom:50%;" />

> 老師、學校無法教你什麼，只能幫你學習---主角沒有開始動作，進入劇情，根本沒有劇

如同很多事情的發展，不見得如我們預期。學習成效/成績也是如此。在這種時候，其實正是我們成長的機會。檢視自己做了什麼，做對了什麼，做錯了什麼，可以有什麼改進，查查資料，問問在這方面比自己強的朋友/同學，請教老師。做些改變，測試一下，然後實行，耐心地給予一些時間讓成果呈現，再重複這個過程。感覺很老調，但一開始做起來並不簡單。學期開始，做些新的嘗試，逐步改造自己的 **非人工智慧系統** ，你會發現這是一個非常有趣好玩的遊戲。

#### 讓每科期末分數加五分的不/偷傳心法

## Where am I? 

* 起始：我是什麼樣的角色？獨特人設？起始裝備與資源？
* 長程：遊戲結束的條件，最終分數計算
* 中短程：重要關卡，短程目標，前期取得可以 Combo 的資源或裝備，常駐能力；有限資源與優先爭取之位置 

<img src="https://www.board-game.co.uk/wp-content/uploads/2021/07/Alma-Mater-Board.jpg" style="zoom:80%;" />

## Mise en place
＊ 各就各位，準備好 -- 在適當的時機與地點，做最能發揮效果的動作

![](https://codingwithknives.files.wordpress.com/2014/08/miseenplace.jpg)

## Layer/Onion/Buffet Reading

<img src="https://www.weekendnotes.com/im/002/01/hyatt-hotel-canberra-easter-buffet-easter-sunday-b1.jpg" style="zoom:50%;" />

## Effective Learning

Mike and Matty: Study more effectively not just harder.
- [The REAL Reason Why You Get Bad Grades](https://youtu.be/GJ_o-1bfz-M) (Worst)
- [Evidence based learning strategies](https://youtu.be/UEJmgaFQUH8) (Best)
- **Worst** because they are on inputs only
  - Re-reading
  - Highlighting
  - Summarizing
  - Mnemonics
- **Best** because they are difficult 
   Brain/Neural Network building
  - Quizzing (Active recall)
  - Spacing (Spaced repetition)
  - Mixing (interleaving/Cross-training)

More at [Smart Learning](https://chtsao.gitlab.io/i2p2022/posts/hello2/) Effective learning

## Elon Musk's Secret

[Learning anything fast](https://www.inc.com/jessica-stillman/heres-elon-musks-secret-for-learning-anything-fast.html)

> "One bit of advice: it is important to view knowledge as sort of a  semantic tree -- make sure you understand the fundamental principles,  i.e. the trunk and big branches, before you get into the leaves/details  or there is nothing for them to hang on to."

提醒：不要太過實際以至於不切實際----求急強快可能反而適得其反。
