---
title: "Week 2 and 3: Point Estimation: A closer look"
date: 2023-02-24T02:29:50+08:00
draft: false
categories: [notes]
tags: [average, expectation, variance, mean square error, unbiased, bias]
markup: "mmark"
---

![Average Taiwan Female Face](http://arshba.ru/images/Facial_Averages/averagetaiwanesefemale.jpg)

### 用平均估計平均？好嗎？

* 這裡所說的(兩個)*平均*是什麼？
* 所謂*好*是什麼意思？

具體來說，若想要估計一個銅板出現正面的機率。我們可能會先丟幾次銅板，然後以出現正面的比率來猜。這似乎是一個大家都會做，而且好像正確的事。真是如此嗎？如果真是這樣，這樣做又好在那裡？

### 攪和
舉 $\bar{x}, 0.3, x_1$ 為例，並以平方誤差 square error 比較，我們無法確切地說
$(\bar{x}- p)^2$ 比 $(0.3-p)^2, (x_1 - p)^2$ 小。兩個困擾：p 不知道，$\bar{x}, x_1$ 會隨抽樣而變化，每次實驗各有不同。

### 統計的問法與回答
也就是，單看**數字結果**無法說好壞。因此，我們改看**方法**，也就是 $\bar{X}, 0.3, X_1$ 在長期使用的整體表現，mean square error (MSE)。這樣可以得到
* $$MSE(\bar{X}, p) = E[(\bar{X}- p)^2] \leq MSE(X_1, p) = E[(X_1 - p)^2] $$

* $\bar{X}, X_1$ are *unbiased* for p, i.e. $E(\bar{X})=p, E( X_1)=p$ for any $p.$ and 0.3 is an *bias* estimator for $p$ with Bias(0.3, p) =$E(0.3)-p.$ 

  以上的結論以 unbiased 的要求直接排除 0.3, 而在 $\bar{X}, X_1$ 比較，以MSE 準則評估，而得到 $$\bar{X}$$ 是三者中最佳的判定。

這個回答與論述也許不能人人滿意，但也不容易提出更好的替代方案（除非妳接受Bayesian 的論述）。但這些等式不等式得來不易。我們必須知道這些定義、演算與推導

1. $X_1, \cdots, X_n \sim_{iid} $. iid= independent + (identical distributed)
2. Let $a$'s be real numbers. Define $Y= \sum_{i=1}^n a_i X_i$. Compute $$E(Y), Var(Y)$$. Moreover, derive/identify the distribution of $Y$.
3. Among them, $\bar{X}$ is simply a special case where $a_i= \frac{1}{n}.$

Let us start with Bernoulli just to get our feet wet or get our hands dirty :)

Prop. Let $X_1, \cdots, X_n \sim_{iid}$ Bernoulli(p) where $p \in [0,1]$ and $p+q=1$. Then 

1. $ E(\bar{X})= E(X_1) = p, \qquad Var(\bar{X})= \frac{Var(X_1)}{n}= \frac{p q}{n} $
2. $n\bar{X}= \sum_{i=1}^n X_i \sim Bin(n, p).$

### Definitions and Concepts
* parameter, paramter space
* estimator, estimator
* mean square error, unbiased, bias 
#### 閱讀與延伸

* Point Estimation: DKLM (our textbook, see [About](https://chtsao.gitlab.io/stat23/about/)) 17.1, 17.2, 17.3; 19.1, 19.2, 20.3
* 基礎機率複習：DKLM Ch. 7, Ch. 8, Ch. 9, Ch. 10, Ch. 11. 
