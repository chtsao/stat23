---
title: "Week 7, Maximum Likelihood II"
date: 2023-03-29T02:53:43+08:00
draft: false
categories: [notes]
tags: [MLE, MLe, log likelihood, normal mean, normal variance, uniform]
---
<img src="https://thumbor.bigedition.com/sunday-afternoon-on-la-grande-jatte/SqSo6zU0CSPEbgBpNAk29cn8MGo=/800x599/filters:format(webp):quality(80)/granite-web-prod/12/f6/12f66c0aa1a04b758879892823b987a1.jpg" style="zoom: 67%;" />

## Maximum Likelihood Estimator


1. $X_1, \ldots, X_n \sim_{iid} N(\mu, \sigma^2)$

   * Find MLE of $\mu \in \mathcal{R}=(-\infty, \infty)$  while $\sigma^2$ is known, $\sigma^2>0$. 
   * Find MLE of $\sigma^2 >0$ while $\mu$ is known, $\mu \in \mathcal{R}.$
   * Find MLE of ($\mu, \sigma^2$) (both unknown) and $\mu \in \mathcal{R}$ and $\sigma^2>0.$
2. Let $X_1, \ldots, X_n \sim_{iid} U(0,\theta), \theta >0.$ Find Maximum Likelihood Estimator of $\theta.$ 

   MLE $\hat{\theta} (\bf{X}) =X_{(n)}=\max(X_1, \ldots, X_n).$ Compare and contrast with $\bar{\bf{X}}$.
3.  [Reading] $X_1, \ldots, X_n \sim_{iid} Exp(\lambda), \lambda>0$
   * Find MLE using $L(\lambda)$ directly, Text P. 318. 
   * Find MLE via $l(\lambda)= lnL(\lambda).$  And compare these two approaches. QuickExercise. 21.3

### Log Likelihood 

When it takes differentiation to find MLe, log likelihood, $l(\theta)=ln(L(\theta))$, is often easier to work with.   Examples:   	
* Finding MLE for Bernoulli($p$). 
* Finding MLE for $N(\mu, \sigma^2).$

### Reference and Links

* Textbook: Ch. 21 Maximum likelihood, particularly, Sec 21.2, Sec. 21.3. 

### Homework 2 (To be discussed on 4/6 (Thr))
* Textbook: Sec. 20.5: 20.1, 20.2. 20.8; Sec 21.6: 21.5, 21.6, 21.9.